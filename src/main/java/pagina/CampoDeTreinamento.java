package pagina;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class CampoDeTreinamento {

	@Ignore
	@Test
	public void cadastroSimples() {
		System.setProperty("webdriver.chrome.driver", "/home/isra/Downloads/chromedriver");
		
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		
		driver.get("file:///home/isra/eclipse-workspace/PraticasTeste/componentes.html");
		
		WebElement nome = driver.findElement(By.id("elementosForm:nome"));
		WebElement sobrenome = driver.findElement(By.id("elementosForm:sobrenome"));
		WebElement sexoM = driver.findElement(By.id("elementosForm:sexo:0"));
		WebElement comida = driver.findElement(By.id("elementosForm:comidaFavorita:2"));
		WebElement comida2 = driver.findElement(By.id("elementosForm:comidaFavorita:0"));
		
		nome.clear();
		nome.sendKeys("Israel");
		
		sobrenome.clear();
		sobrenome.sendKeys("Araujo");
		
		sexoM.click();
		
		comida.click();
		comida2.click();
		
		WebElement element = driver.findElement(By.id("elementosForm:escolaridade"));
		Select combo = new Select(element);
		combo.selectByValue("superior");
		
		
		Assert.assertEquals("Superior", combo.getFirstSelectedOption().getText());
		driver.quit();
		
	}
	@Test
	public void brincarAlert() {
		System.setProperty("webdriver.chrome.driver", "/home/isra/Downloads/chromedriver");
		
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("file:///home/isra/eclipse-workspace/PraticasTeste/componentes.html");
		
		WebElement clickAlert = driver.findElement(By.xpath("//table[@id='elementosForm:tableUsuarios']//tr[1]/td[3]/input"));
		
		clickAlert.click();
		
		Alert alert = driver.switchTo().alert();
		String texto = alert.getText();
		Assert.assertEquals("Francisco", texto);
		alert.accept();
		
		driver.switchTo().window("");
		
		driver.quit();
	}
	
}
